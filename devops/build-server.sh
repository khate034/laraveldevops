#!bin/bash/

apt -qy update
apt -qy install unzip
apt -qy install zip
apt-get -qy install git-core
apt-get -qy install curl
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

docker-php-ext-install pdo pdo_mysql ctype bcmath zip

curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir+/usr/local/bin --filename=composer

apt -qy install npm
